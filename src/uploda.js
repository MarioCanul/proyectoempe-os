const multer = require('multer');
const path  = require('path');
const mime = require('mime');
const fs = require('fs');
// Creamos las opciones
   
    var storage = multer.diskStorage({
        destination: function(req, file,callback){
            
            var dir = "./frontend/imagenes";
            if(!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }
            callback(null,dir);
        },
        filename:function(req,file,callback){
            callback(null,file.originalname);
        }
      })
const upload = multer({storage: storage,
}); // <= aqui asignamos las opciones

module.exports = upload; // <= exportamos el módul
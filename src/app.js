const express = require('express');
const app = express();
const cors= require('cors');
const bodyParser = require('body-parser');
const upload =require('./uploda');
const path = require('path');
//settings
app.set('port',process.env.PORT);
//midedlewares
app.use(express.static('./frontend/imagenes'));
app.use(bodyParser.json())
app.use(cors());
app.use(express.json());
app.use(upload.array('photos',5));
//routes
app.use('/Proyecto/Objeto', require('./routes/Empeños'));
if (process.env.NODE_ENV === 'production') {
    // Set static folder
    app.use(express.static('frontend/build'));

    const path1 = path.resolve(__dirname, '..', 'frontend', 'build', 'index.html');
    console.log(path1);
    app.get('*', (req, res) => {
        res.sendFile(path1);
    });
}

module.exports = app;
//http://localhost:4000/Proyecto/Objeto/




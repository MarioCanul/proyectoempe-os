const con = require('../database')
const cloudinary = require('cloudinary');
require('dotenv').config();
cloudinary.config({
    // cloud_name: 'nemeantube',
    cloud_name: process.env.ClOUD_NAME,
    // api_key: '923694955644928',
    api_key: process.env.API_KEY,
    // api_secret: 'UCZpOfUg1W2BCi899ssCj9ybWqo'
    api_secret: process.env.API_SECRET
});
const fs = require('fs-extra');
const UserCtrl = {};

function Deletephoto(public_id) {// funcion que borra las imagenes del cloudinary update, delete
    public_id.forEach(d => {
        if (d.imagenemp) {

            cloudinary.uploader.destroy(d.imagenemp, (result) => { console.log('Foto eliminada :' + result) });
        } else {

            cloudinary.uploader.destroy(d.nombre_img, function (result) { console.log(result) });
        }
    });
}

UserCtrl.getEmpeños = async (req, res) => {
    con.query("SELECT * FROM empeños", (err, rows, fields) => {
        if (!err) {
            // console.log('getEmpeños rows: ', rows);
            res.json(rows)
            //res.send(rows);
        } else {
            console.log(err);
        }
    });
};



UserCtrl.getEmpeniosIMG = async (req, res) => {
    // console.log("Entro")
    con.query('SELECT * FROM imagenes WHERE id_Emp=?', [req.params.id_empe], (err, rows, fields) => {
        if (!err) {
            // console.log('getEmpeniosIMG rows: ', rows);
            res.json(rows)
            //res.send(rows);
        } else {
            console.log(err);
        }
    })

};
UserCtrl.getEmpeño = async (req, res) => {
    // console.log("Entro")
    con.query('SELECT * FROM empeños WHERE id_Objeto=?', [req.params.Id_Objeto], (err, rows, fields) => {
        if (!err) {
            // console.log('getEmpeniosIMG rows: ', rows);
            res.json(rows)
            //res.send(rows);
        } else {
            console.log(err);
        }
    })

};

UserCtrl.createEmpeño = async (req, res) => {
    // console.log(req);
    var files = req.files;
    console.log(files);
    precio = req.body.precio;
    console.log("PRECIO: ", precio);
    descripcion = req.body.Descripcion;
    categoria = req.body.categoria
    const result = await cloudinary.v2.uploader.upload(files[0].path, { width: 168, height: 300, quality_analysis: true }, (result) => { console.log('Foto subida :' + result) }).catch(e => {
        console.log('tenemos un problema.')
        console.log(e);
        throw e
    })
    console.log(result);
    var values = [
        [result.url, precio, descripcion, categoria, result.public_id]
    ]
    await con.query("INSERT INTO empeños (foto, precio, descripsion,categoria,nombre_img) VALUES ?", [values], (err, rows, fields) => {
        if (!err) {
            var imagen = new Array(files.length);
            for (var i = 0; i < files.length; i++) {
                cloudinary.v2.uploader.upload(files[i].path)
                    .then((result) => {
                        var valuesimg = [
                            [rows['insertId'], result.public_id, result.url]
                        ]
                        // console.log(valuesimg)
                        con.query("INSERT INTO imagenes(id_Emp,imagenemp,img_url) VALUES ?", [valuesimg], (err, rows, fields) => {
                            if (!err) {//resultados de la tabla imagenes

                            } else {
                                console.log(err);
                            }
                        })
                    })
                    .catch((error) => {
                        console.log(error);
                    });
                fs.unlink(files[i].path).catch((error) => { console.log(error + ' img minis'); });
            }
            // fs.unlink(files[0].path).catch((error)=>{ console.log(error+' img main'); });
            res.json(rows)
        } else {
            console.log(err);
        }
    })
};
UserCtrl.getUsuario = async (req, res) => {
    con.query('SELECT * FROM admin WHERE usuario=?', [req.params.usuario], (err, rows, fields) => {
        if (!err) {
            res.send(rows);
            console.log(rows)
        } else {
            console.log(err);
        }
    })
}
UserCtrl.getEmpeñoscat = async (req, res) => {
    //    console.log("Entro en categoria")
    con.query('SELECT * FROM empeños WHERE categoria=?', [req.params.Categoria], (err, rows, fields) => {
        if (!err) {
            res.json(rows)
            //res.send(rows);
        } else {
            console.log(err);
        }
    })

}

UserCtrl.DeleteEmpeño = async (req, res) => {

    await con.query('SELECT * FROM empeños WHERE id_Objeto=?', [req.params.id], (err, rows, fields) => {
        // namephoto(rows[0].nombre_img); 
        Deletephoto(rows);
    })
    await con.query('SELECT * FROM imagenes WHERE id_Emp=?', [req.params.id], (err, rows, fields) => {
        Deletephoto(rows);
    })

    await con.query('DELETE FROM empeños WHERE id_Objeto=?', [req.params.id], (err, rows, fields) => {
        if (!err) {
            con.query('DELETE FROM imagenes WHERE id_Emp=?', [req.params.id], (err, rows, fields) => {
                if (!err) {
                } else {
                    console.log(err);
                }
            })
            res.json(rows);
        } else {
            console.log(err);
        }
    })
}

UserCtrl.updateEmpeño = async (req, res) => {
    // console.log("Entro al controller")
    var files = req.files;
    precio = req.body.precio;
    descripcion = req.body.Descripcion;
    categoria = req.body.categoria;
    foto = req.body.foto;
    nombre = req.body.nombre;
    if (files.length !== 0) {
        await con.query('SELECT * FROM empeños WHERE id_Objeto=?', [req.params.id], (err, rows, fields) => {
            // namephoto(rows[0].nombre_img); 
            Deletephoto(rows);
        })
        await con.query('SELECT * FROM imagenes WHERE id_Emp=?', [req.params.id], (err, rows, fields) => {
            Deletephoto(rows);
        })

        const result = await cloudinary.v2.uploader.upload(files[0].path, { width: 180, height: 240, crop: "fill" }, (result) => { console.log('Foto actualizada :' + result) });

        await con.query("UPDATE empeños set foto=? ,precio=?, descripsion=?, categoria=?, nombre_img=? WHERE id_Objeto=?", [result.url, precio, descripcion, categoria, result.public_id, req.params.id], (err, rows, fields) => {
            if (!err) {
                con.query('DELETE FROM imagenes WHERE id_Emp=?', [req.params.id], (err, rows, fields))
                var imagen = new Array(files.length)
                for (var i = 0; i < files.length; i++) {
                    cloudinary.v2.uploader.upload(files[i].path, { width: 180, height: 240, crop: "fill" }, (result) => { console.log('Foto actualizada :' + result) })
                        .then((result1) => {
                            var valuesimg = [
                                [req.params.id, result1.public_id, result1.url]
                            ]
                            con.query("INSERT INTO imagenes(id_Emp,imagenemp,img_url) VALUES ?", [valuesimg], (err, rows, fields) => {
                                if (!err) {//resultados de la tabla imagenes   
                                } else {
                                    console.log(err);
                                }
                            })
                        })
                        .catch((error) => { ('Error en la subida') })
                    fs.unlink(files[i].path).catch((error) => { console.log(error + ' img minis'); });
                }
                res.json(rows)
            } else {
                console.log(err);
            }
        })
    }
    else {
        await con.query("UPDATE empeños set foto=? ,precio=?, descripsion=?, categoria=?, nombre_img=? WHERE id_Objeto=?", [foto, precio, descripcion, categoria, nombre, req.params.id], (err, rows, fields) => {
            if (!err) {
                res.json(rows)
            } else {
                console.log(err);
            }
        })

    }

}
module.exports = UserCtrl;

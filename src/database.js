var mysql = require('mysql');
require('dotenv').config();

// console.log(process.env.HOST);
// console.log(process.env.USER);
// console.log(process.env.PASSWORD);
// console.log(process.env.DATABASE);

// var con = mysql.createConnection({
//   host: process.env.HOST,
//   user: process.env.USER,
//   password: process.env.PASSWORD,
//   database: process.env.DATABASE,
//   multipleStatements: true,
// });

var con = mysql.createPool({
  host: process.env.HOST,
  user: process.env.USER,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  multipleStatements: true,
});

con.getConnection(function(err, connection) {
  if (err) throw err; // not connected!
  else {
    console.log("DB is Connected!");
  }
});
  
// con.connect((err) => {
//   if (!err) {
//     console.log("DB is Connected!");
//   }
//   else {
//     console.log(err);
//   }
// });

module.exports = con

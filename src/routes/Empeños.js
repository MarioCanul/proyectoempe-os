const { Router } = require('express');
const router = Router();
const { createEmpeño, getEmpeñoscat, getEmpeño, getEmpeniosIMG, getUsuario, getEmpeños, updateEmpeño, DeleteEmpeño } = require('../controllers/Empeños.controllers');
router.route('/')
    .get(getEmpeños)
    .post(createEmpeño)
router.route('/emp/:id_empe')
    .get(getEmpeniosIMG)
router.route('/:id')
    .put(updateEmpeño)
    .delete(DeleteEmpeño)
router.route('/Categoria/:Categoria')
    .get(getEmpeñoscat);
router.route('/Empenio/:Id_Objeto')
    .get(getEmpeño);
router.route('/login/:usuario')
    .get(getUsuario);
module.exports = router;

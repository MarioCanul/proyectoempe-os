import React, { Component } from "react";
// import EmailIcon from '@material-ui/icons/Email';
// import RingVolumeIcon from '@material-ui/icons/RingVolume';
import PhoneIcon from '@material-ui/icons/Phone';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
// import Lana from "../assets/img/lana_logo.png";
import Logo from '../assets/img/LanaLogo.svg'
import "../assets/css/Footer.css";
export default class Alumno extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="logoscontenido">
          <div className="logoscontenido_first logo">
            <img
              className="logo_lana"
              src={Logo}
              height="90"
              width="160"
              alt="fotito"
            />
          </div>
          <div className="logoscontenido_second">
            {/* <div
              className="logoscontenido_second_item"
              style={{ marginBottom: "10px" }} >
              <EmailIcon className="logo_carta"/>
              <span className="correo">mario_can12@hotmail.com</span>
            </div> */}
            <div className="logoscontenido_second_item">
              <PhoneIcon
                className="logo_telefono"

              />
              {/* <img
                className="logo_telefono"
                src={RingVolumeIcon}
                height="30"
                width="40"
                alt="fotito"
              /> */}
              <span className="telefono">9995 44 03 95</span>
            </div>
          </div>
          <div className="logoscontenido_third">
            <div className="network">
              <a href="https://www.facebook.com/lanadevolada">
                <FacebookIcon className="logofacebook" />
              </a>
            </div>
            <div className="network">
              <InstagramIcon className="logoinstagram" />
            </div>
          </div>
        </div>
        <div className="about">
          Copyright &copy; Lana De Volada
        </div>
        {/* <div className="about">
          <a href="/">Contacto de la Empresa </a>
          <a href="/">Terminos de uso</a>
          <a href="/">Declaración de privacidad</a>
          <a href="/">Centro de ayuda</a>
        </div> */}
      </footer>
    );
  }
}

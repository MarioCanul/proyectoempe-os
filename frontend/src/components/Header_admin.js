
import React, { Component } from 'react';
import Logo from '../assets/img/LanaLogo.svg'
import '../assets/css/Header_Admin.css'
import SearchIcon from '@material-ui/icons/Search';
// import TextField from '@material-ui/core/TextField';

export default class Alumno extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Id_Objeto: null
        }
    }
    async onSubmit(e) {
        e.preventDefault();
        var Id_Objeto = this.state.Id_Objeto;
        console.log(Id_Objeto);
        if (Id_Objeto) {
            await this.props.buscar(Id_Objeto);
            this.setState({ Id_Objeto: null })
        }

        else {
            await this.props.empeños();
        }
    }
    render() {

        return (
            <div className='header_Admin '>
                <div className="Logos parte1s"><img src={Logo} alt="logo"></img></div>
                <div className="Titulos parte2s"><span>ADMINISTRACION DE PRODUCTOS</span></div>

                <div className="buscador">
                    <form autoComplete="off" onSubmit={this.onSubmit.bind(this)} >

                        <input type="text" class="form-control" onChange={(e) => this.setState({ Id_Objeto: e.target.value })} id="standard-basic" aria-describedby="idHelp" placeholder="Enter Id" />
                        {/* <TextField id="standard-basic" onChange={(e) => this.setState({ Id_Objeto: e.target.value })} color="red" label="Buscar Por Id" variant="filled" /> */}
                        <button type="submit"><SearchIcon /></button>
                    </form>
                </div>
            </div>

        );
    }
}
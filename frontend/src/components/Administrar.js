import React, { Component } from 'react';
import Footer from './Footer';
import HeaderAdmin from './Header_admin';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import '../assets/css/Administrar.css'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { SERVER } from '../variables';
import { deleteCookie } from '../Cookies'
import { Link } from 'react-router-dom'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import ReactHtmlParser from 'react-html-parser';
// import { useHistory } from "react-router-dom";
import { CrearEmpeño, EditarEmpeño, EliminarEmpeño, Carrousel } from './Modales'
import Axios from 'axios';
// import Image from '../../../backend/imagenes'
export default class Administrador extends Component {
  constructor(props) {
    super(props)
    this.state = {
      redirect: "/Login",
      Data: [],
      itemselect: [],
      imgEmpeños: [],
      Edit: false,
      Delete: false,
      carussel: false,
      Registrar: false,
      lenght: "",
      page: 1,
      rowsPerPage: 5
    }
    this.search = this.search.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.getEmpeños = this.getEmpeños.bind(this);
    this.DeleteEmpeño = this.DeleteEmpeño.bind(this);
    this.EditEmpeño = this.EditEmpeño.bind(this);
    this.CrearEmpeño = this.CrearEmpeño.bind(this);
    this.OpenCar = this.OpenCar.bind(this);
  }

  componentDidMount() {
    this.getEmpeños();
  }
  async search(Id_Objeto) {
    const res = await Axios.get(SERVER + 'Proyecto/Objeto/Empenio/' + Id_Objeto);
    this.setState({
      Data: res.data
    })
  }
  async getEmpeños() {
    const res = await Axios.get(SERVER + 'Proyecto/Objeto/');
    this.setState({
      Data: res.data,
      length: res.data['length']
    })
    // console.log(this.state.Data)  
  }

  handleClose() {
    this.setState({
      Edit: false,
      Delete: false,
      Registrar: false,
      carussel: false

    })
  }
  DeleteEmpeño(d) {
    this.setState({
      Delete: true,
      itemselect: d
    })
  }

  async EditEmpeño(d) {
    var id_empe = d.id_Objeto
    const res = await Axios.get(SERVER + 'Proyecto/Objeto/emp/' + id_empe);
    this.setState({
      Edit: true,
      imgEmpeños: res.data,
      itemselect: d
    })
  }

  CrearEmpeño() {
    this.setState({
      Registrar: true,
    })
  }
  async OpenCar(d) {
    var id_empe = d.id_Objeto
    const res = await Axios.get(SERVER + 'Proyecto/Objeto/emp/' + id_empe);
    // console.log(res.data);
    this.setState({
      carussel: true,
      imgEmpeños: res.data,
    })
  }
  CerraSesion() {
    // console.log("Entra a Cerrar Sesion")
    deleteCookie('login');
  }

  render(d) {

    const Data = this.state.Data;
    const dat = Data.map((d) => (<TableRow className="itemsTable" key={d.id_Objeto}>
      <TableCell align="center" className="itemscat">{d.id_Objeto}</TableCell>
      <TableCell align="center" className="itemsimg">
        <Button color="secondary" className="btncarousel" onClick={e => (this.OpenCar(d))}>
          <img width="90%" height="90%" src={d.foto} alt="imagen" />
        </Button>
      </TableCell>
      <TableCell align="center" className="itemsprecio"><AttachMoneyIcon fontSize="small" className="singmoney" />{parseFloat(d.precio).toFixed(2)}</TableCell>
      <TableCell align="center">{ReactHtmlParser(d.descripsion)}</TableCell>
      <TableCell align="center" className="itemscat">{d.categoria}</TableCell>
      <TableCell align="center" className="itemscat">
        <Button variant="contained" color="primary" className="btnedit" onClick={e => (this.EditEmpeño(d))}>
          <EditIcon />
        </Button>
      </TableCell>
      <TableCell align="center" className="itemscat">
        <Button variant="contained" color="secondary" className="btndelete" onClick={e => (this.DeleteEmpeño(d))}>
          <DeleteIcon />
        </Button>
      </TableCell>
    </TableRow>));
    var Modalcarousel = this.state.carussel === true ? <Carrousel cerrar={this.handleClose} imagenes={this.state.imgEmpeños} /> : '';
    var ModalR = this.state.Registrar === true ? <CrearEmpeño cerrar={this.handleClose} objeto={this.getEmpeños} /> : '';
    var ModalD = this.state.Delete === true ? <EliminarEmpeño Data={this.state.itemselect} cerrar={this.handleClose} empe={this.getEmpeños} /> : '';
    var ModalE = this.state.Edit === true ? <EditarEmpeño Data={this.state.itemselect} imagenes={this.state.imgEmpeños} cerrar={this.handleClose} empe={this.getEmpeños} /> : '';
    return (

      <div>
        <HeaderAdmin buscar={this.search} empeños={this.getEmpeños} />
        <div className="container">
          <div className="Contenedorbtn">

            <div className="Añadir">
              <span> AGREGAR : </span>
              <Button variant="contained" className="btnAñadir" color="primary" onClick={e => (this.CrearEmpeño())}>
                <AddIcon />
              </Button>
            </div>
            <div className="CerrarSesion">
              <Link to="/Login">
                <Button variant="contained" className="btnCerrar" color="primary" onClick={e => (this.CerraSesion())}>
                  Cerar Sesion
                 </Button>
              </Link>
            </div>
          </div>

          {Modalcarousel}
          {ModalR}
          {ModalD}
          {ModalE}
          <TableContainer component={Paper} className="TableContainer">
            <Table className="table" aria-label="customized table">
              <TableHead>
                <TableRow >
                  <TableCell align="center" className="titulosTable">Id</TableCell>
                  <TableCell className="titulosTable">Foto</TableCell>
                  <TableCell align="center" className="titulosTable">Precio</TableCell>
                  <TableCell align="center" className="titulosTable">Descripcion</TableCell>
                  <TableCell align="center" className="titulosTable"> Categoria</TableCell>
                  <TableCell align="center" className="titulosTable">Editar</TableCell>
                  <TableCell align="center" className="titulosTable">Eliminar</TableCell>
                </TableRow>
              </TableHead>
              <TableBody className="bodytable">
                {dat}
              </TableBody>
            </Table>
          </TableContainer>
        </div>

        <Footer />
      </div>
    );
  }
}
import Axios from 'axios';
import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import{Exitosa,NoExitosa} from './Modales'
import { Component } from 'react';
import { setCookie } from '../../Cookies';
import Logo from '../../assets/img/LanaLogo.svg'
import {SERVER} from '../../variables';
import '../../assets/login.css';

export default class SignIn extends Component {
 constructor(props){
  super(props);
  this.handleClose=this.handleClose.bind(this);
  this.link=this.link.bind(this);
  this.state={
    dataM:[],
    ItemSelect:null,
    SesionExitosa:false,
    SesionnoExitosa:false,
  }
 }

 handleClose(){
  this.setState({
    SesionnoExitosa:false,
  })
 }

  SignIn = async (e) => {
    e.preventDefault();
    let usuario = document.getElementById("Usuario").value;
    let pass = document.getElementById("password").value;
    await Axios.get(SERVER+'Proyecto/Objeto/login/' + usuario)
      .then(response => {
        if (response.data['length'] > 0) {

          var Usuario = response.data[0]['usuario'];
          var password = response.data[0]['pass'];

          if (Usuario === usuario && password === pass) {
            this.setState({
              dataM: response.data,
              SesionExitosa: true,
            })
          }
          else {
            this.setState({
              SesionnoExitosa: true,
            })
          }
        }
        else {
          // console.log(" NO HAY DATOS")
          this.setState({
            SesionnoExitosa: true,
          })
        }
        // console.log(response.data)
        return response.data;
      })
      .catch(error => {
        return error;
      });

    // console.log()
  }
  
   link () {
    // console.log("Entro al")
    var usuario = this.state.dataM[0]['usuario'];
  setCookie('login',usuario,1)
    // console.log("Administrador")
    this.setState({
      SesionExitosa:false,
    });
    window.location='/Administrador/';
  }

  render(){
   
    var modalExitosa = this.state.SesionExitosa === true ? <Exitosa cerrar={this.link}/>:'';
    var modalNoExitosa = this.state.SesionnoExitosa === true ? <NoExitosa cerrar={this.handleClose}/>:'';
  return (
    <div className="Container" >
       {modalExitosa}
      {modalNoExitosa}
      <div className="Container_login clearfix">
                        <div className="item_login" >
                              <div className="card_login" >
                                <img className="card-img-top" src={Logo} height="200" width="240" alt="fotito"/>
                                <span className="titulo">INICIAR SESION</span>
                                <div className="card-body">
                                <form className="form" autoComplete="off">
                                              <TextField
                                              className="textfield"
                                                variant="outlined"
                                                // margin="normal"
                                                required
                                                id="Usuario"
                                                // label="Usuario" 
                                              />
                                              <TextField
                                              className="textfield"
                                                // variant="outlined"
                                                // margin="normal"
                                                required
                                                variant="outlined"
                                                //  label="Constraseña"
                                                type="password"
                                                id="password"
                                              />
                                              <Button
                                                type="button"
                                                variant="contained"
                                                color="primary"
                                                className="submit"
                                                onClick={this.SignIn}  
                                              >Iniciar</Button>
                                              <br></br>
          
                                   </form>
                                </div>
                             </div>
    
                          </div>
          </div>
      
    </div>
    
  );
}
}
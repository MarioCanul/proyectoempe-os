import React, { Component } from 'react';
import Axios from 'axios';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
// import CardMedia from '@material-ui/core/CardMedia';
import Card from '@material-ui/core/Card';
import { SERVER } from '../variables';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import '../assets/modales.css'
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// import {blobToFile} from '../Cookies'
import ImgDropAndCrop from './ImgDropAndCrop';
// import ReactCrop from 'react-image-crop';
// import 'react-image-crop/dist/ReactCrop.css';


export class CrearEmpeño extends Component {
  constructor(props) {
    super(props);
    this.imageCropCanvasRef = React.createRef();
    this.state = {
      data: undefined,
      age: undefined,
      cropImg: []
    };
  }

  handleOnChange = (e, editor) => {
    const data = editor.getData();
    this.setState({ data })
  }
  ImgCroped = async (crop) => {
    if (this.state.cropImg.length >= 5) {
      alert("La cantidad permitida son 5 Archivos");
    }
    else {
      await this.setState({ cropImg: [...this.state.cropImg, crop] });
      // console.log(this.state.cropImg.length);
    }
  }
  onChange = editorState => this.setState({ editorState });

  async onSubmit(e) {
    e.preventDefault()
    var form = document.getElementById("uploadform")
    var Descripcion = this.state.data
    var formdata = new FormData(form);
    formdata.append('Descripcion', Descripcion);
    formdata.append('photos', this.state.cropImg[0]);
    formdata.append('photos', this.state.cropImg[1]);
    formdata.append('photos', this.state.cropImg[2]);
    formdata.append('photos', this.state.cropImg[3]);
    formdata.append('photos', this.state.cropImg[4]);

    await Axios.post(SERVER + 'Proyecto/Objeto/', formdata, {//envio con post
      headers: {
        'Content-Type': 'multipart/form-data'
      },
    })
    this.props.objeto();
    this.props.cerrar();
  };

  save = (data) => {
    // console.log(data);
  }

  render() {
    return (
      <Dialog fullWidth
        aria-labelledby="alert-dialog-title1"
        aria-describedby="alert-dialog-description1"
        open={true}
        onClose={this.handleClose}
      >
        <DialogTitle id="alert-dialog-title1">{"Nuevo Empeño "}</DialogTitle>
        <DialogContent id="alert-dialog-description1">
          <form autoComplete="off" className="wid" id="uploadform" onSubmit={this.onSubmit.bind(this)} encType="multipart/form-data">

            <TextField
              required
              step="0.01"
              min="0"
              max="10"
              type="decimal"
              name="precio"
              label="Precio del Empeño"
              id="precio"
              variant="outlined"
              fullWidth
            /><br /><br />
            <CKEditor
              editor={ClassicEditor}
              onChange={this.handleOnChange}
            />
            <br /><br />
            <InputLabel id="demo-simple-select-label">Tipo de Categoria</InputLabel >
            <Select

              name="categoria"
              fullWidth
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={this.state.age || ""}
              onChange={(e) => { this.setState({ age: e.target.value }) }}
            >
              <MenuItem value="Computadoras">Computadoras/Laptops</MenuItem>
              <MenuItem value="Electrodomesticos">Electrodomesticos</MenuItem>
              <MenuItem value="Joyeria">Joyeria</MenuItem>
              <MenuItem value="Linea blanca">Linea blanca</MenuItem>
              <MenuItem value="Tablets">Tablets</MenuItem>
              <MenuItem value="TV">TV/Pantallas</MenuItem>
              <MenuItem value="Otros">Otros</MenuItem>
            </Select>
            <br /><br />

            <ImgDropAndCrop ref={this.imageCropCanvasRef} handleCropimg={this.ImgCroped} />

            <br /><br />
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.cerrar} color="primary">
            Cerrar
            </Button>
          <Button type="submit" form="uploadform" color="primary">
            Guardar
            </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export class EliminarEmpeño extends Component {

  delete = () => {
    // console.log("entre a delete")
    // e.preventDefault();
    // console.log(this.props.Data)
    var id = this.props.Data.id_Objeto;
    Axios.delete(SERVER + 'Proyecto/Objeto/' + id);
    this.props.cerrar();
    this.props.empe();
  }
  render() {
    return (
      <Dialog
        //descripcion
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        open={true}
        onClose={this.handleClose}
      >
        <DialogTitle id="alert-dialog-title">Seguro que quieres Eliminar el Empeño</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            - La Eliminacion borrara el Empeño de la base de datos-
            </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={e => { this.props.cerrar() }} color="primary">
            Cerrar
            </Button>
          <Button onClick={this.delete} color="primary">
            Borrar
          </Button>
        </DialogActions>
      </Dialog>

    )
  }
}

export class EditarEmpeño extends Component {
  constructor(props) {
    super(props)
    this.imageCropCanvasRef = React.createRef();
    this.state = {
      cropImg: [],
      id: this.props.Data.id_Objeto,
      precio: this.props.Data.precio,
      descripcion: this.props.Data.descripsion,
      imagen: null,
      age: this.props.Data.categoria
    }
  }
  handleOnChange = (e, editor) => {
    const data = editor.getData();
    this.setState({ descripcion: data })
    // console.log(this.state.descripcion);
  }
  ImgCroped = async (crop) => {
    if (this.state.cropImg.length >= 5) {
      alert("La cantidad permitida son 5 Archivos");
    }
    else {
      await this.setState({ cropImg: [...this.state.cropImg, crop], imagen: true });
      // console.log(this.state.cropImg.length);
    }
  }
  onSubmit = async (e) => {
    e.preventDefault()
    var form = document.getElementById("uploadform");
    var Descripcion = this.state.descripcion
    var formdata = new FormData(form);
    formdata.append('Descripcion', Descripcion)

    if (this.state.imagen === true) {
      formdata.append('photos', this.state.cropImg[0]);
      formdata.append('photos', this.state.cropImg[1]);
      formdata.append('photos', this.state.cropImg[2]);
      formdata.append('photos', this.state.cropImg[3]);
      formdata.append('photos', this.state.cropImg[4]);
      await Axios.put(SERVER + 'Proyecto/Objeto/' + this.props.Data.id_Objeto, formdata, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
      })

      this.props.empe();

    }
    else {
      var foto = this.props.Data.foto;
      var nombre = this.props.Data.nombre_img;
      formdata.append('foto', foto)
      formdata.append('nombre', nombre,)
      await Axios.put(SERVER + 'Proyecto/Objeto/' + this.props.Data.id_Objeto, formdata, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
      })
      this.props.empe()
    }
    this.props.cerrar()

  }
  render() {
    const Data = this.props.imagenes;
    const dat = Data.map((d, index) => (
      <img width="150" height="200" src={d.img_url} key={index} alt="imagen" />
    ))
    const responsive = {
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 1,
        slidesToSlide: 1 // optional, default to 1.
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 3,
        slidesToSlide: 2 // optional, default to 1.
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1 // optional, default to 1.
      }
    };
    return (
      <Dialog fullWidth
        aria-labelledby="alert-dialog-title1"
        aria-describedby="alert-dialog-description1"
        open={true}
        onClose={this.handleClose}
      >
        <DialogTitle id="alert-dialog-title1">{"Nuevo Empeño "}</DialogTitle>
        <DialogContent id="alert-dialog-description1">
          <form autoComplete="off" className="wid" id="uploadform" onSubmit={this.onSubmit.bind(this)} encType="multipart/form-data">
            <TextField
              type="number"
              name="precio"
              label="Precio del Empeño"
              id="precio"
              value={this.state.precio}
              onChange={(e) => this.setState({ precio: e.target.value })}
              variant="outlined"
              fullWidth
            /><br /><br />

            <CKEditor
              editor={ClassicEditor}
              onChange={this.handleOnChange}
              data={this.state.descripcion}
            />
            <br /><br />
            <Select
              name="categoria"
              fullWidth
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={this.state.age}
              onChange={(e) => this.setState({ age: e.target.value })}
            >
              <MenuItem value="Computadoras">Computadoras/Laptops</MenuItem>
              <MenuItem value="Electrodomesticos">Electrodomesticos</MenuItem>
              <MenuItem value="Joyeria">Joyeria</MenuItem>
              <MenuItem value="Linea blanca">Linea blanca</MenuItem>
              <MenuItem value="Tablets">Tablets</MenuItem>
              <MenuItem value="TV">TV/Pantallas</MenuItem>
              <MenuItem value="Otros">Otros</MenuItem>
            </Select><br /><br />
            <Card className="cartaimg">
              <Carousel
                // swipeable={true}
                // draggable={true}
                // showDots={true}
                responsive={responsive}
                // ssr={true} // means to render carousel on server-side.
                // infinite={true}
                transitionDuration={500}
                containerClass="carousel-container"
                // removeArrowOnDeviceType={["tablet", "mobile"]}
                deviceType={this.props.deviceType}
                dotListClass="custom-dot-list-style"
                itemClass="carousel-item-padding-40-px"
              >
                {dat}
              </Carousel>
            </Card>
            <br /><br />
            <ImgDropAndCrop ref={this.imageCropCanvasRef} handleCropimg={this.ImgCroped} />
            {/* <InputLabel htmlFor="inputFileServer">Cargar Foto De Empeño</InputLabel> */}
            {/* <input  id="inputFileServer"  name="photos"  type="file" onChange={(e) =>this.setState({imagen:e.target.files}) } multiple/> */}
            <br /><br />
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.cerrar} color="primary">
            Cerrar
                    </Button>
          <Button type="submit" form="uploadform" color="primary">
            Guardar
                    </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export class Carrousel extends Component {
  render() {
    const Data = this.props.imagenes;
    const dat = Data.map((d, index) => (
      <img width="80%" height="90%" key={index} src={d.img_url} alt="imagen" />

    ));
    const responsive = {
      desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 2,
        slidesToSlide: 2 // optional, default to 1.
      },
      tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        slidesToSlide: 2 // optional, default to 1.
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1 // optional, default to 1.
      }
    };
    return (

      <Dialog
        //descripcion
        className="dialog-carrussel"
        fullWidth
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        open={true}
        onClose={this.handleClose}
      >
        <DialogTitle id="alert-dialog-title">Fotos</DialogTitle>
        <DialogContent>

          <Carousel
            swipeable={true}
            draggable={false}
            showDots={true}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={false}
            // autoPlay={this.props.deviceType !== "mobile" ? true : false}
            // autoPlaySpeed={1000}
            // keyBoardControl={true}
            // customTransition="all .5"
            transitionDuration={500}
            containerClass="carousel-container"
            // removeArrowOnDeviceType={["tablet", "mobile"]}
            deviceType={this.props.deviceType}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
          >
            {dat}
          </Carousel>
        </DialogContent>
        <DialogActions className="carrusselbtn" >
          <Button onClick={e => { this.props.cerrar() }} color="primary" >
            Cerrar
            </Button>
        </DialogActions>
      </Dialog>

    )
  }
}
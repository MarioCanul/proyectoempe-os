
import React, { Component } from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
// import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import SearchIcon from '@material-ui/icons/Search';
import Logo from '../assets/img/LanaLogo.svg'
import '../assets/css/Header.css'

export default class Alumno extends Component {
    constructor(props) {
        super(props)

        this.state = {
            categoria: ''
        }
    }
    async onSubmit(e) {
        e.preventDefault();
        // console.log(this.state.categoria)
        if (this.state.categoria === "Todos")
            await this.props.empeños();
        else {
            await this.props.buscar(this.state.categoria)
            // await this.props.buscar(e)
        }
    }
    render() {

        return (
            <div className='header '>
                <div className="Logo parte1"><img src={Logo} alt="logo"></img></div>
                <div className="Titulo parte2"><span>CATALOGO DE PRODUCTOS</span></div>

                <div className="buscador parte3">
                    <form autoComplete="off" onSubmit={this.onSubmit.bind(this)} >

                        <Select
                            fullWidth
                            className="select"
                            displayEmpty
                            id="Select"
                            value={this.state.categoria}
                            onChange={(e) => this.setState({ categoria: e.target.value })}
                        >
                            <MenuItem className="MenuItem" value="" disabled>ELIGE LA CATEGORIA</MenuItem>
                            <MenuItem className="MenuItem" value="Computadoras">Computadoras/Laptops</MenuItem>
                            <MenuItem className="MenuItem" value="Electrodomesticos">Electrodomesticos</MenuItem>
                            <MenuItem className="MenuItem" value="Joyeria">Joyeria</MenuItem>
                            <MenuItem className="MenuItem" value="Linea blanca">Linea blanca</MenuItem>
                            <MenuItem className="MenuItem" value="Tablets">Tablets</MenuItem>
                            <MenuItem className="MenuItem" value="TV">TV/Pantallas</MenuItem>
                            <MenuItem className="MenuItem" value="Otros">Otros</MenuItem>
                            <MenuItem className="MenuItem" value="Todos">Todos</MenuItem>
                        </Select>
                        <button type="submit" className="btn btn-warning glass"><SearchIcon /></button>
                    </form>
                </div>
            </div>
        );

    }
}
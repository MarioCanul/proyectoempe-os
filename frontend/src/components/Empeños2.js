import React, { Component } from 'react'
import Axios from 'axios';
import Footer from './Footer';
import Header from './Header';
import { SERVER } from '../variables'
import '../assets/css/Empeños2.css'
import Button from '@material-ui/core/Button';
import { Carrousel } from '../components/Modales';
import ReactHtmlParser from 'react-html-parser';
// import AttachMoneySharpIcon from '@material-ui/icons/AttachMoneyRounded';
// import AttachMoneySharpIcon from '@material-ui/icons/MonetizationOnOutlined';
export default class Empeños2 extends Component {
    constructor(props) {
        super(props)
        this.handleClose = this.handleClose.bind(this);
        this.search = this.search.bind(this);
        this.getEmpeños = this.getEmpeños.bind(this);
        this.OpenCar = this.OpenCar.bind(this);
        this.state = {
            Data: [],
            imgEmpeños: [],
            carussel: false,
        }
    }

    componentDidMount() {
        this.getEmpeños();
    }

    handleClose() {
        this.setState({
            carussel: false
        })
    }

    async OpenCar(d) {
        var id_empe = d.id_Objeto
        const res = await Axios.get(SERVER + 'Proyecto/Objeto/emp/' + id_empe);
        // console.log(res.data);
        this.setState({
            carussel: true,
            imgEmpeños: res.data,
        })
    }

    async search(Categoria) {
        // console.log("entra en el search");
        // console.log(Categoria);
        const res = await Axios.get(SERVER + 'Proyecto/Objeto/Categoria/' + Categoria);
        // console.log(res.data);
        this.setState({
            Data: res.data
        })
    }

    async getEmpeños(e) {
        const res = await Axios.get(SERVER + 'Proyecto/Objeto/');
        this.setState({
            Data: res.data
        })
        // console.log(this.state.Data)
    }

    render() {
        var VerificarData = this.state.Data.length === 0 ?
            <div> <h3 className="Leyenda">'No se encontraron Articulos de esta Categoria' </h3> </div>
            : this.state.Data.map(d => (
                <div className="item" key={d.id_Objeto}>
                    <div className="card" >
                        <div className="id_Objeto">{d.id_Objeto}</div>
                        <Button color="secondary" className="btncarousel" onClick={e => (this.OpenCar(d))}>
                            <img className="card-img-top" src={d.foto} alt="fotito" />
                        </Button>

                        <div className="card-bodys ">
                            <h5 className="card-title">${parseFloat(d.precio).toFixed(2)}</h5>
                            <div className="card-text scroll">{ReactHtmlParser(d.descripsion)}</div>
                            <div className="card-footer">
                                <span>9995 44 03 95</span>
                                {/* <button className="btn btn-danger" onClick={() => this.CrearC()}>
                                    Llama
                                </button> */}
                            </div>
                        </div>

                    </div>
                </div>
            ));


        var Modalcarousel = this.state.carussel === true ? <Carrousel cerrar={this.handleClose} imagenes={this.state.imgEmpeños} /> : '';
        return (
            <div >
                <Header buscar={this.search} empeños={this.getEmpeños} />
                <div className="listItems clearfix">
                    {VerificarData}
                    {Modalcarousel}
                </div>
                <Footer />
            </div>

        )
    }
}
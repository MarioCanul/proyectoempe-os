import React, { Component } from 'react'

// import Dropzone from 'react-dropzone'
import ReactCrop from 'react-image-crop'
import 'react-image-crop/dist/ReactCrop.css';

import {base64StringtoFile,
    
    extractImageFileExtensionFromBase64,
    image64toCanvasRef} from './ResualbleUtils'

const imageMaxSize = 1000000000 // bytes
const acceptedFileTypes = 'image/x-png, image/png, image/jpg, image/jpeg, image/gif'
const acceptedFileTypesArray = acceptedFileTypes.split(",").map((item) => {return item.trim()})
class ImgDropAndCrop extends Component {
    constructor(props){
        super(props)
        this.imagePreviewCanvasRef = React.createRef()
        this.fileInputRef = React.createRef()
        this.state = {
            imgName:null,
            imgSrc: null,
            imgSrcExt: null,
            crop: {
                aspect: 1/1,
                unit: '%',
                width: 100,
                height: 100,
            }
        }
    }

    verifyFile = (files) => {
        if (files && files.length > 0){
            const currentFile = files[0]
            const currentFileType = currentFile.type
            const currentFileSize = currentFile.size
            if(currentFileSize > imageMaxSize) {
                alert("This file is not allowed. " + currentFileSize + " bytes is too large")
                return false
            }
            if (!acceptedFileTypesArray.includes(currentFileType)){
                alert("This file is not allowed. Only images are allowed.")
                return false
            }
            return true
        }
    }

    handleOnDrop = (files, rejectedFiles) => {
        if (rejectedFiles && rejectedFiles.length > 0){
            this.verifyFile(rejectedFiles)
        }


        if (files && files.length > 0){
             const isVerified = this.verifyFile(files)
             if (isVerified){
                 // imageBase64Data 
                 const currentFile = files[0]
                //  console.log(currentFile);
                 const myFileItemReader = new FileReader()
                 myFileItemReader.addEventListener("load", ()=>{
                     // console.log(myFileItemReader.result)
                     const myResult = myFileItemReader.result
                     this.setState({
                         imgSrc: myResult,
                         imgSrcExt: extractImageFileExtensionFromBase64(myResult)
                     })
                 }, false)

                 myFileItemReader.readAsDataURL(currentFile);

             }
        }
    }


    handleImageLoaded = (image) => {
        // console.log(image);
    }
    handleOnCropChange = (crop) => {
        // console.log(crop);
        this.setState({crop:crop});
    }
    handleOnCropComplete = (crop, pixelCrop) =>{
        // console.log(crop, pixelCrop)
        const canvasRef = this.imagePreviewCanvasRef.current
        const {imgSrc}  = this.state
        image64toCanvasRef(canvasRef, imgSrc, crop);
    }
    handleDownloadClick = async (event) => {
        event.preventDefault()
        const {imgSrc}  = this.state
        if (imgSrc) {
            const canvasRef = this.imagePreviewCanvasRef.current
        
            const {imgSrcExt,imgName} =  this.state
            const imageData64 = canvasRef.toDataURL('image/' + imgSrcExt)
            const myFilename =imgName+ imgSrcExt

            // file to be uploaded
           const myNewCroppedFile = base64StringtoFile(imageData64, myFilename)
            // console.log(myNewCroppedFile)
            // download file
            this.props.handleCropimg(myNewCroppedFile);
            // downloadBase64File(imageData64, myFilename);
            // this.handleClearToDefault()
            this.handleClearToDefault();
        }
        
        
    }

    handleClearToDefault = event =>{
        if (event) event.preventDefault()
        const canvas = this.imagePreviewCanvasRef.current
        const ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        this.setState({
            imgName:null,
            imgSrc: null,
            imgSrcExt: null,
            crop: {
                aspect: 1/1,
                unit: '%',
                width: 100,
                height: 100
            }

        })
        // this.fileInputRef.current.value = null;
    }

    handleFileSelect = event => {
        // console.log(event)
        const files = event.target.files
        if (files && files.length > 0){
              const isVerified = this.verifyFile(files)
             if (isVerified){
                 // imageBase64Data 
                 const currentFile = files[0]
                 const myFileItemReader = new FileReader()
                 myFileItemReader.addEventListener("load", ()=>{
                    //  console.log(myFileItemReader.result)
                     const myResult = myFileItemReader.result
                    //  console.log(myResult);
                     this.setState({
                         imgName:currentFile.name,
                         imgSrc: myResult,
                         imgSrcExt: extractImageFileExtensionFromBase64(myResult)
                     })
                 }, false)

                 myFileItemReader.readAsDataURL(currentFile)

             }
        }
    }
  render () {
      const {imgSrc} = this.state
    return (
      <div>
        <h1>Imagen a subir</h1>
        {imgSrc !== null ? 
            <div>
               

                 <ReactCrop 
                     src={imgSrc} 
                     crop={this.state.crop} 
                     onImageLoaded={this.handleImageLoaded}
                     onComplete = {this.handleOnCropComplete}
                     onChange={this.handleOnCropChange}/>

                  <br/><br/>
                  <p>Preview Canvas Crop </p>
                  <br/>
                  <canvas ref={this.imagePreviewCanvasRef} ></canvas>
                  <br/><br/>
                  <button onClick={this.handleDownloadClick}>Listo</button>
                  <button onClick={this.handleClearToDefault} >Clear</button>
              </div>

           : 
           <input ref={this.fileInputRef} type='file' accept={acceptedFileTypes} multiple={false} onChange={this.handleFileSelect} />
            //  <Dropzone onDrop={this.handleOnDrop} accept={acceptedFileTypes} multiple={false} maxSize={imageMaxSize}>Drop image here or click to upload</Dropzone>
         }
        
      </div>
    )
  }
}

export default ImgDropAndCrop
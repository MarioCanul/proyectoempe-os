import React from 'react';
import { Redirect } from 'react-router-dom'
import { BrowserRouter as Router ,Route, Switch} from 'react-router-dom'
import Administrar from './components/Administrar'
import Empeños2 from './components/Empeños2'
import Login from './components/login/Login'
import {getCookie} from './Cookies'
import NoMatches from './assets/img/404/Monster 404 Error-amico.svg'
// import CropEditor from './components/CropEditor'
import './App.css';

function App() {
  function myHighOrder( myComponent) {
    var login =getCookie('login');
    if (login) {
      return myComponent;
    } else {
      return () => {
        return <Redirect to='/Login' />
      };
    }
  }
  const NoMatch = ()=>(
    <div className="Error">
      <img    src={NoMatches} alt="imagen"/>
    </div>
  )
  return (
    <Router>
      <Switch>
      <Route path="/Login" exact component={Login} />
        <Route path="/Administrador" exact component={myHighOrder(Administrar)} />
        <Route path="/"  exact component={Empeños2} />
        {/* <Route path="/"  exact component={CropEditor} /> */}
        <Route  component={NoMatch} />
      </Switch>
    </Router>
  );
}
export default App;
